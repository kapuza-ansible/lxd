# Control lxd by ansible
## Variables
* lxd_action - name of operation
  * delete - delete vm
  * generate_hosts - generate /tmp/hosts by inventory
  * inventory - generate list of all RUNNING vm in remote and add to virtual inventory (register: lxd_hosts, lxd_ip)
  * launch - launch new vm(lxd_container) in remote(lxd_remote) by lxd_image_remote:lxd_image
  * openvpn - add /dev/net/tun to vm (for start openvpn)
  * stop - stop vm(lxd_container)
  * help - display help to terminal
* lxd_remote - remote for start vm (lxd_remote: 'local')
* lxd_image_remote - image remote (lxd_image_remote: 'local')
* lxd_image - default image (lxd_image: 'ubuntu16')
* lxd_domain - default home domain for generate hosts (lxd_domain: 'home.xmu')
* lxd_container - container name

## Install
```bash
ansible-galaxy install git+https://gitlab.com/kapuza-ansible/lxd.git
```

## Examples
```yaml
---
- name: Examples
  hosts: localhost
    roles:
      - role: lxd
        lxd_action: 'launch'
        lxd_image: 'centos7'
        lxd_container: 'sso-support'

      - role: lxd
        lxd_action: 'openvpn'
        lxd_container: 'sso-support'

      - role: lxd
        lxd_action: 'stop'
        lxd_container: 'sso-support'

      - role: lxd
        lxd_action: 'delete'
        lxd_container: 'sso-support'
```

